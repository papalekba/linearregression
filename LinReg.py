# -*- coding: utf-8 -*-
"""
Created on Thu Sep 12 21:00:13 2019

@author: rstbb
"""

import numpy as np
class LinearRegression():
    def __init__(self, X, y, alpha=0.03, n_iter=1500):

        self.alpha = alpha
        self.n_iter = n_iter
        self.n_samples = len(y)
        self.n_features = np.size(X, 1)
        self.X = X
        self.y = y
        self.w = np.zeros((self.n_features, 1))
        self.coef_ = None
        self.intercept_ = None

    def fit(self, lambda1 = 0, lambda2 = 0):
        # Here we use gradient decent as our fit method
        # Let use l1_lambda or l2_lambda to perform l1 or l2 regularization
        # if both are provided the regularization will be l1 and l2
        
        if lambda2 > 0 and lambda2 > 0:
            print("L1 and L2 regularization with lambda1, lambda2:", [lambda1, lambda2])
        elif lambda1 > 0:
            print("L1 regularization with lambda1:", lambda1)
        elif lambda2 > 0:
            print("L2 regularization with lambda2:", lambda2)
        else:
            print("No regularization performed.")
        
        for i in range(self.n_iter):
            self.w = self.w - (self.alpha/self.n_samples) * \
            (self.X.T @ (self.X @ self.w - self.y ) + lambda1*self.w + \
            lambda2*self.w)
                

        self.intercept_ = self.w[0]
        self.coef_ = self.w[1:]

        return self

    def accuracy(self, X=None, y=None):

        if X is None:
            X = self.X
        else:
            X = X

        if y is None:
            y = self.y
        else:
            y = y

        y_pred = X @ self.w
        score = np.mean(1 - (((y - y_pred)**2).sum() / ((y - y.mean())**2).sum()))

        return score

    def predict(self, X):
        y = X @ self.w
        return y

    def get_w(self):

        return self.w

